package tests.consumer;

import functionalInterface.supplier.driver.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public final class GetTextLinkTest {


    @Test
    public void getGoogleTextLinks(){
        WebDriver driver = DriverFactory.getDriver("firefox");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.google.com");

        List<WebElement> elements = driver.findElements(By.tagName("abc"));
        for(WebElement e : elements){
            if(e.getText().trim().length() > 0)
                System.out.println(e.getText());
        }

        /*driver.findElements(By.tagName("a"))
                .stream()
                .filter(e -> !e.getText().isEmpty())
                .filter(e -> !e.getText().contains("s"))
                .map(e -> e.getText().toUpperCase(Locale.ROOT))
                .forEach(e -> System.out.println(e));*/

        // Improvement
       /* driver.findElements(By.tagName("a"))
                .stream()
                .map(WebElement::getText)
                .filter(str -> str.length() > 0)
                .filter(str -> !str.contains("s"))
                .map(String::toUpperCase)
                .forEach(System.out::println);*/
        driver.quit();

    }
}
