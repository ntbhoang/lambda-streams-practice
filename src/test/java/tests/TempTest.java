package tests;
import org.testng.Assert;
import org.testng.annotations.Test;


public final class TempTest extends BaseTest {


    @Test
    public void test(){
        driver.get(baseUrl);
        String expectedTitle = "Welcome: Mercury Tours";
        String actualTitle = driver.getTitle();
        Assert.assertEquals(actualTitle, expectedTitle);
    }
}
