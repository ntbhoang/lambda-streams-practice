package tests;

import functionalInterface.supplier.driver.DriverFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

public class BaseTest {
    static WebDriver driver;
    String baseUrl = "https://www.google.com";

    protected BaseTest(){}

    @BeforeTest
    public void setUp(){
        driver = DriverFactory.getDriver("firefox");
        init();
    }

    @AfterTest
    public void tearDown(){
        DriverFactory.quit();
    }

    private static void init(){
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }
}
