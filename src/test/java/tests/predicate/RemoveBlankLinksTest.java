package tests.predicate;

import functionalInterface.supplier.driver.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import tests.predicate.rules.Rule;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class RemoveBlankLinksTest {


    @Test
    public void getGoogleTextLinks(){
        WebDriver driver = DriverFactory.getDriver("firefox");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();


        driver.get("https://www.google.com");
        List<WebElement> elements = driver.findElements(By.tagName("a"));
        elements.removeIf(webElement -> webElement.getText().isEmpty());
        elements.forEach(webElement -> System.out.println(webElement.getText()));

        /*driver.findElements(By.tagName("a"))
                .stream()
                .filter(e -> !(e.getText().isEmpty()))
                .forEach(e -> System.out.println(e.getText()));*/


        Predicate<WebElement> p1 = (we) -> !we.getText().isEmpty();
        driver.findElements(By.tagName("a"))
                .stream()
                .filter(p1)
                .forEach(webElement -> System.out.println(webElement.getText()));
        driver.quit();

    }

    @Test
    public void removeLinkContainsS(){
        WebDriver driver = DriverFactory.getDriver("firefox");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        driver.get("https://www.google.com");
        /*driver.findElements(By.tagName("a"))
        .stream()
        .filter(Rule.get().get(1))
                .forEach(e -> System.out.println(e.getText()));*/
        List<WebElement> elements =  driver.findElements(By.tagName("a"));
        Rule.get().forEach(rule -> elements.removeIf(rule));
    }
}
