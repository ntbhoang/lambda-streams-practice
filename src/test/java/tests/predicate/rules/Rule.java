package tests.predicate.rules;

import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Predicate;

public final class Rule {
    private static final Predicate<WebElement> isBlank = (e) -> e.getText().isEmpty();
    private static final Predicate<WebElement> containsS = (e) -> e.getText().toLowerCase(Locale.ROOT).contains("s");
    private static List<Predicate<WebElement>> rules = new ArrayList<>();

    public static List<Predicate<WebElement>> get(){
        rules.add(isBlank);
        rules.add(containsS);
        return rules;
    }
}
