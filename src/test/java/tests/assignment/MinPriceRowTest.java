package tests.assignment;

import functionalInterface.supplier.driver.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import pages.TableDemoPage;

import java.util.Comparator;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.concurrent.TimeUnit;

public class MinPriceRowTest {

    @Test
    public void minPriceRowTest(){
        WebDriver driver = DriverFactory.getDriver("firefox");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        driver.get("https://vins-udemy.s3.amazonaws.com/java/html/java8-stream-table-price.html");

                 Optional  minPrice = driver.findElements(By.xpath("//td[@class='price']"))
                         .stream()
                         .map(e -> Integer.parseInt(e.getText()))
                         .peek(n -> System.out.println(n))
                         .min(Comparator.comparingInt(n -> n));

        System.out.println(minPrice.get());
        driver.quit();
    }
}
