package tests.assignment;

import functionalInterface.supplier.driver.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.TableDemoPage;
import tests.BaseTest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

public class CheckboxAssignment {
    private static TableDemoPage demoPage;
    private Predicate<List<WebElement>> male;
    private Predicate<List<WebElement>> female;
    private Predicate<List<WebElement>> allGender;
    private Predicate<List<WebElement>> allAU;
    private Predicate<List<WebElement>> femaleAU;

    @Test(dataProvider = "testData")
    public void checkBoxAssignment(Predicate<List<WebElement>> searchCriteria){
        WebDriver driver = DriverFactory.getDriver("firefox");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        demoPage = new TableDemoPage(driver);
        demoPage.goTo();
        demoPage.selectRows(searchCriteria);
        driver.quit();
    }

    @DataProvider
    public Object[] testData(){
        return new Object[] {
                male = (l) -> l.get(1).getText().equalsIgnoreCase("male"),
                female = (l) -> l.get(1).getText().equalsIgnoreCase("female"),
                allAU = (l) -> l.get(2).getText().equalsIgnoreCase("AU"),
                femaleAU = female.and(allAU),
                //allGender = male.and(female)
        };
    }


}
