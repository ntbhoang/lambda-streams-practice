package tests.stream;

import functionalInterface.supplier.driver.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.predicate.rules.Rule;
import utils.LinkUtil;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class BrokenLinkTest {

    @Test
    public void brokenLinkTest(){
        WebDriver driver = DriverFactory.getDriver("firefox");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        driver.get("https://the-internet.herokuapp.com/broken_images");
        driver.findElements(By.tagName("img")).stream()
                .map(e -> e.getAttribute("src")) // Stream<String>
                .forEach(src ->
                    System.out.println(LinkUtil.getResponseCode(src) + " ::  " + src)
                );
        driver.quit();
    }

    @Test
    public void brokenLinkAnyMatchTest(){
        WebDriver driver = DriverFactory.getDriver("firefox");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        driver.get("https://the-internet.herokuapp.com/broken_images");
        boolean result = driver.findElements(By.tagName("img")).stream()
                .map(e -> e.getAttribute("src")) // Stream<String>
                .map(src -> LinkUtil.getResponseCode(src)) // Stream<Code>
                .anyMatch(rc -> rc != 200);
        Assert.assertTrue(result);
        driver.quit();
    }

    @Test
    public void brokenLinkUsingCollectTest(){
        WebDriver driver = DriverFactory.getDriver("firefox");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        driver.get("https://the-internet.herokuapp.com/broken_images");
        List<String> resultInfo = driver.findElements(By.tagName("img")).stream()
                .map(e -> e.getAttribute("src")) // Stream<String>
                .parallel()
                .filter(src -> LinkUtil.getResponseCode(src) != 200) // Stream<Code>
                .collect(Collectors.toList());

        Assert.assertEquals(resultInfo.size(), 0, resultInfo.toString());
        driver.quit();
    }
}
