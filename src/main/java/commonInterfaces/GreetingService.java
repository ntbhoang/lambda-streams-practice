package commonInterfaces;

// A functional interface is a SAM - single abstract method interface

@FunctionalInterface
public interface GreetingService {
    String greet(String name);
}
