package lambda;

import commonInterfaces.GreetingService;

import java.util.Locale;

public class HowLambdaExecuted {
    public static void main(String[] args) {
        System.out.println("Started Execution");

        //receiveNumber( getNumber() );

        test(name -> {
            System.out.println("Inside lambda");
            return name.toUpperCase(Locale.ROOT);
        });

        System.out.println("Ending Execution");

    }

    private static int getNumber(){
        System.out.println("Called getNumber()");
        return 5;
    }

    private static void receiveNumber(int i){
        System.out.println("Receive Number is :: " + i);
    }

    private static void test(GreetingService g){
        System.out.println("Inside the test method. Receive the greeting service");
        String returnedStr = g.greet("test");
        System.out.println(returnedStr);
    }
}
