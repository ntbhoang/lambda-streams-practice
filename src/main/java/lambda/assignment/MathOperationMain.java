package lambda.assignment;

public class MathOperationMain {
    public static void main(String[] args) {
        MathOperation add = (x , y) -> x + y;
        MathOperation subtract = (x , y) -> x - y;
        MathOperation divide = (x , y) -> {
           if(y == 0) throw new ArithmeticException("Cannot divide by 0");
           return x / y;
        };
        MathOperation multiply = (x , y) -> x * y;

        add(add, 5, 8);
        subtract(subtract, 5, 8);
        divide(divide, 5, 1);
        multiply(multiply, 5, 8);

        int result = calculate(5, add, 2);
        result = calculate(result, subtract, 3);
        result = calculate(result, multiply, 7);
        result = calculate(result, add, 2);
        result = calculate(result, divide, 3);
        System.out.println(result);

    }

    private static void add(MathOperation op, int x, int y){
        int result = op.operate(x, y);
        System.out.println(result);
    }

    private static void subtract(MathOperation op, int x, int y){
        int result = op.operate(x, y);
        System.out.println(result);
    }

    private static void divide(MathOperation op, int x, int y){
        int result = op.operate(x, y);
        System.out.println(result);
    }

    private static void multiply(MathOperation op, int x, int y){
        int result = op.operate(x, y);
        System.out.println(result);
    }

    private static int calculate(int onScreenNumber, MathOperation op, int enteredNumber){
        return op.operate(onScreenNumber, enteredNumber);
    }
}
