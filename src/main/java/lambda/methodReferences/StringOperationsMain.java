package lambda.methodReferences;

import java.util.Locale;
import java.util.Objects;

public class StringOperationsMain {
    public static void main(String[] args) {
        // StringOperations op1 = (str -> System.out.println(str.toUpperCase(Locale.ROOT)));
        StringOperations op1 = System.out::println;
        StringOperations op2 = String::toUpperCase;
        op1.accept("udemy");
        op2.accept("test");

        StringOperations op3 = str -> str.toUpperCase(Locale.ROOT);


        System.out.println();
    }
}
