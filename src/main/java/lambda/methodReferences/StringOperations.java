package lambda.methodReferences;

@FunctionalInterface
public interface StringOperations {
    void accept(String str);
}
