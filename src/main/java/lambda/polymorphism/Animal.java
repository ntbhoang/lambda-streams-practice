package lambda.polymorphism;

public abstract class Animal {

    protected abstract void makeSound();
}
