package lambda.polymorphism;

public class Cat extends Animal{
    @Override
    protected void makeSound() {
        System.out.println("Meow-meow");
    }
}
