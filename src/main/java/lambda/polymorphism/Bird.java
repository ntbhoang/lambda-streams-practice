package lambda.polymorphism;

public class Bird extends Animal{
    @Override
    protected void makeSound() {
        System.out.println("Chip-chip-chip");
    }
}
