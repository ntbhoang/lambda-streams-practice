package lambda;

import commonInterfaces.GreetingService;

import java.util.Locale;

public class LambdaBasic {
    public static void main(String[] args) {
        GreetingService upperCaseGreeting = name -> name.toUpperCase(Locale.ROOT);
        test(upperCaseGreeting);

        test((n) -> n.toUpperCase(Locale.ROOT) + n.length());

        getLambda().greet("udemy");

    }

    private static void test(GreetingService g){
        String returnedString = g.greet("udemy course");
        System.out.println(returnedString);
    }

    private static GreetingService getLambda(){
        GreetingService g = (s) -> s.toUpperCase(Locale.ROOT);
        return g;
    }
}
