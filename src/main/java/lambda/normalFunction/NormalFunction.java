package lambda.normalFunction;

import java.util.Locale;

public class NormalFunction {
    /*
         With normal function, it tells that:
         - You give me the data, I will take care of the behaviors for you.
         Lambda function is on the opposite, it tells:
         - I have the data, you tells me what to do.
     */
    public static void main(String[] args) {
        strToUpperCase("hoang");
        strToUpperCase("thao");
        strToLowerCase("HUNG");
        strToLowerCase("PHONG");
    }

    private static void strToUpperCase(String str){
        String returnedStr =  str.toUpperCase(Locale.ROOT);
        System.out.println(returnedStr);
    }

    private static void strToLowerCase(String str){
        String returnedStr = str.toLowerCase(Locale.ROOT);
        System.out.println(returnedStr);
    }
}
