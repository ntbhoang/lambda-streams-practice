package functionalInterface.predicate;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.function.*;
import java.util.stream.Collectors;

public class PredicateMain {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Adam", "Alexander", "John");
        List<String> result =  names.stream()
                .filter(name -> name.startsWith("A"))
                .filter(name -> name.length() < 5)
                .collect(Collectors.toList());
        result.forEach(name -> System.out.println(name));

    }
}
