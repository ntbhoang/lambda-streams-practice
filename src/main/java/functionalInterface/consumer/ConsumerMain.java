package functionalInterface.consumer;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class ConsumerMain {
    public static void main(String[] args) {
        Map<String, Consumer<String>> consumerMap = new HashMap<>();
        List<String> names = new ArrayList<>();
        names.add("jack");
        names.add("james");
        names.add("anna");

        Consumer<String> dbConsumer = (String s) -> {
            System.out.println("Added " + s + " into the DB.");
        };

        Consumer<String> loggingConsumer = (String s) -> {
            System.out.println("Logged " + s + " to the log file.");
        };

        Consumer<String> dbLogConsumer = dbConsumer.andThen(loggingConsumer);

        //names.forEach(dbConsumer);
        names.forEach(loggingConsumer.andThen(dbConsumer));

        // Bi-Consumer
        BiConsumer<String, String> biConsumer = (String str1, String str2) -> {
            System.out.println(str1.toUpperCase(Locale.ROOT) + str2.toUpperCase(Locale.ROOT));
        };

        biConsumer.accept("udemy", "lambda course");

        consumerMap.put("dbConsumer", dbConsumer);
        consumerMap.put("loggingConsumer", loggingConsumer);

        consumerMap.forEach((k, v) -> {
            System.out.println("Key is:: " + k);
        });

    }

}
