package functionalInterface.function;

import java.util.Locale;
import java.util.function.Function;

public class FunctionMain {
    public static void main(String[] args) {
        Function<String, Integer> strLen = (str) -> str.length();
        Function<Integer, Integer> square = (i) -> i * i;
        Function<Integer, Integer> plusTwo = (i) -> i + 2;

       /* System.out.println(
                strLen.andThen(square).apply("Test")
        );*/

        System.out.println(
                plusTwo.andThen(square).apply(5) // 49
        );

        System.out.println(
                plusTwo.compose(square).apply(5) // 27
        );


    }
}
