package functionalInterface.supplier.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

public final class DriverFactory {

    private static WebDriver driver;
    private static final Supplier<WebDriver> CHROME_SUPPLIER = () -> {
        System.setProperty("webdriver.chrome.driver", "src/executable/chromedriver");
        driver = new ChromeDriver();
        return driver;
    };
    private static final Supplier<WebDriver> FIREFOX_SUPPLIER = () -> {
        System.setProperty("webdriver.gecko.driver", "src/executable/geckodriver");
        driver = new FirefoxDriver();
        return driver;
    };

    private static final Map<String, Supplier<WebDriver>> DRIVERS = new HashMap<>();

    static {
        DRIVERS.put("chrome", CHROME_SUPPLIER);
        DRIVERS.put("firefox", FIREFOX_SUPPLIER);
    }

    private DriverFactory(){}

    public static WebDriver getDriver(String browser){
        //setDriver(browser);
        return DRIVERS.get(browser.toLowerCase(Locale.ROOT)).get();
    }

    private static void setDriver(String browser){
        /*if(browser.equalsIgnoreCase("firefox")){
            System.setProperty("webdriver.gecko.driver", "src/executable/geckodriver");
            driver = new FirefoxDriver();
        }else if(browser.equalsIgnoreCase("chrome")){
            System.setProperty("webdriver.chrome.driver", "src/executable/chromedriver");
            driver = new ChromeDriver();
        }*/


    }

    public static void quit(){
        if(Objects.nonNull(driver)) driver.quit();
    }



}
