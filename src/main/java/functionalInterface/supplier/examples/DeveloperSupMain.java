package functionalInterface.supplier.examples;

import java.math.BigDecimal;
import java.time.LocalDate;

public class DeveloperSupMain {
    public static void main(String[] args) {
        Developer developer = new Developer();
        developer.setName("Hoang");
        developer.setSalary(new BigDecimal("4000.0000"));
        developer.setStartDate(LocalDate.now());
        System.out.println(developer);

        Developer dev1 = DeveloperFactory.factory(Developer::new);
        Developer dev2  = DeveloperFactory.factory(() -> new Developer("Jack"));

        System.out.println(dev1);
        System.out.println(dev2);


    }

}
