package functionalInterface.supplier.examples;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Developer {
    private String name;
    private BigDecimal salary;
    private LocalDate startDate;

    public Developer(){}
    public Developer(String name){this.name = name;}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    @Override
    public String toString(){
        return "{name:" + this.name + ", salary: " + this.salary + ", startDate: " + this.startDate + "}";
    }
}
