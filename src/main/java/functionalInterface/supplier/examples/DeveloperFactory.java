package functionalInterface.supplier.examples;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;
import java.util.function.Supplier;

public class DeveloperFactory {

    public static Developer factory(Supplier<? extends Developer> sup){
        Developer dev = sup.get();
        if(Objects.isNull(dev.getName()) || "".equals(dev.getName())) dev.setName("Hoang");
        dev.setSalary(BigDecimal.ONE);
        dev.setStartDate(LocalDate.of(2021, 07, 26));
        return dev;
    }
}
