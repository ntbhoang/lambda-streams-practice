package streams;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class TerminalOperations {
    public static void main(String[] args) {
        List<String> fruits = new ArrayList<>();
        fruits.add("apple");
        fruits.add("orange");
        fruits.add("strawberry");
        fruits.add("lime");

        // count()
        // I have `size()` method, why should I use `count()`
        // `size()` always returns the number ele in  the list
        // `count()` can be used with other stream method such as `filter()`
        System.out.println(
                fruits.stream()
                        .filter(str -> str.length() > 6)
                        .count()
        );

        // Find first and Optional Type
        System.out.println(
                fruits.stream()
                        .filter(str -> str.length() > 4)
                        .findFirst()

        );

        Optional<String> opt = fruits.stream()
                                        .filter(str -> str.length() > 4)
                                        .findFirst();

        if(opt.isPresent()) System.out.println(opt.get());

        System.out.println(
                fruits.stream()
                        .min(Comparator.reverseOrder())
        );

        System.out.println(
                fruits.stream()
                        .filter(str -> str.length() > 4)
                        .anyMatch(str -> str.contains("s"))

        );

    }
}
