package streams;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(46);
        numbers.add(10);
        numbers.add(31);
        numbers.add(17);
        numbers.add(22);
        numbers.add(90);
        numbers.add(90);
        //squareOfEvenNumber(numbers);
        squareOfEvenNumberStream(numbers);
    }

    private static void squareOfEvenNumber(List<Integer> numbers){
        int square = 0;
        List<Integer> squares = new ArrayList<>();
        for(int i = 0; i < numbers.size(); i++){
            if(numbers.get(i) % 2 == 0){
                square = numbers.get(i) * numbers.get(i);
                squares.add(square);
            }
            if(squares.size() == 3){
                break;
            }
        }
        squares.forEach(i -> System.out.println(i));
    }

    private static void squareOfEvenNumberStream(List<Integer> numbers){
        numbers.stream()
                .peek(n -> System.out.println("Filter - received :: " + n))
                .filter(n -> n % 2 == 0)
                //.sorted()
                .peek(n -> System.out.println("Map - received :: " + n))
                .map(n -> n * n)
                .limit(3)
                .forEach(i -> System.out.println(i));
    }
}
