package streams;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

public class StreamReduceAndIntStream {
    public static void main(String[] args) {
        // Problem: We cannot count sum of a stream
        // Solution 1:  use `reduce`
        List<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(4);
        numbers.add(5);

        int sum = numbers.stream()
                .reduce((x, y) -> {
                    System.out.println("before return x + y::: " + x + " " + y);
                    return x + y;
                }).get();
        System.out.println(sum);

       int sum1 = numbers.stream()
               .mapToInt(n -> n)
               .sum();
        System.out.println(sum1);

        // To replace for for loop we can use IntStream
        for(int i = 0; i < 10; i++) {
            System.out.println("This is increasing i::: " + i);
        }

        IntStream.range(1, 10)
                .forEach(i -> {
                    System.out.println("This is increasing i::: " + i);
                });

    }
}
