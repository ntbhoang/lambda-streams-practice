package streams;

import java.util.ArrayList;
import java.util.List;

public class StreamOperation {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(4);
        numbers.add(10);
        numbers.add(31);
        numbers.add(17);
        numbers.add(22);
        numbers.add(90);
        squareOfEvenNumberStream(numbers);



    }

    private static void squareOfEvenNumberStream(List<Integer> numbers){
        numbers.stream()
                .filter(n -> {
                    System.out.println("Filter - received :: " + n);
                    return n % 2 == 0;
                })
                .map(n -> {
                    System.out.println("Map - recevived :: " + n);
                    return n * n;
                })
                .limit(3);
                //.forEach(i -> System.out.println(i));
    }
}
