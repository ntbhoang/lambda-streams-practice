package streams;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class FlatMapStream {
    public static void main(String[] args) {
        /*
            - Use for List<List<T>>
         */

        List<List<Integer>> arr = new ArrayList<>();
        List<Integer> arr1 = new ArrayList<>();
        arr1.add(1);
        arr1.add(3);
        arr1.add(5);
        List<Integer> arr2 = new ArrayList<>();
        arr2.add(4);
        arr2.add(8);
        arr2.add(12);

        arr.add(arr1);
        arr.add(arr2);

        System.out.println(
                arr.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList())
        );
    }
}
