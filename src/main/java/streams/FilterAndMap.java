package streams;

import java.util.ArrayList;
import java.util.List;

public class FilterAndMap {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(46);
        numbers.add(10);
        numbers.add(31);
        numbers.add(17);
        numbers.add(22);
        numbers.add(90);

        numbers.stream()
                .peek(n -> System.out.println("Filter - received::: " + n))
                .filter(n -> n % 2 == 0)
                .peek(n -> System.out.println("Filter - received even number:::" + n))
                .filter(n -> n > 5)
                .peek(n -> System.out.println("Filter - received even number great than 5:::" + n))
                .map(n -> (n * n) + 2)
                .forEach(n -> System.out.println(n));

    }
}
