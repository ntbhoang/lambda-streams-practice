package streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TerminalOperationCollect {
    public static void main(String[] args) {
        List<String> fruits = new ArrayList<>();
        fruits.add("apple");
        fruits.add("orange");
        fruits.add("strawberry");
        fruits.add("lime");

        Map<Integer, List<String>> map = fruits.stream()
                .collect(Collectors.groupingBy(String::length));
        System.out.println(map);

        Map<Character, List<String>> map1 = fruits.stream()
                .collect(Collectors.groupingBy(s -> s.charAt(0)));
        System.out.println(map1);

        String test = "Maiden";
        System.out.println(test.length());

    }
}
