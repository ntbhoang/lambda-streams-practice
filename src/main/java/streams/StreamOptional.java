package streams;

import java.util.Optional;

public class StreamOptional {
    public static void main(String[] args) {
        String a = "Apple";
        String b = "Orange";
        String c = "Lime";

        Optional<String> smallest = getSmaller(a, b, c);
        if(smallest.isPresent()) System.out.println(smallest.get());

    }

   /* private static String getSmaller(String s1, String s2, String s3){
        return s1;
        // This method might return null
    }*/

    private static Optional<String> getSmaller(String s1, String s2, String s3){
        return Optional.ofNullable(null);
        // This method might return null
    }

}
