package assignment;

import java.util.*;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        // Calculator class: a Map with all the possible operations `Map<String, MathOperation>
        // Calculator has a `calculate` method which accepts an String expression such as `5 + 2 - 3 * 7 + 2 /3`
        // `calculate` method will split the expression by the giving space - identify what need to be done the return the result
        // Calculator class should support new operations without modifying the class
        // 5+4*3


        String expr = "5 + 2 - 3 * 7 + 2 ^ 3";
        String expr1 = "10 / 2 * 3 - 3 / 3 * 5";
        String expr2 = "9 * 9 - 1 * 7 / 8 + 30";

        Calculator.addOperation("^", (x, y) -> (int)Math.pow(x, y));

        int result = Calculator.calculate(expr);


        //int result = calculate(expr);
        System.out.println(result);
    }

    private static int calculate(String expr){
        List<String> expStr = Arrays.asList(expr.split(" "));
        int len = expStr.size();
        int result = Integer.parseInt(expStr.get(0));
        for(int i = 2; i < len; i += 2){
            int tempValue = Integer.parseInt(expStr.get(i));
            for(int j = i - 1; j < i; j += 2){
                String op = expStr.get(j);
                switch (op){
                    case "+":
                        result += tempValue;
                        break;
                    case "-":
                        result -= tempValue;
                        break;
                    case "*":
                        result *= tempValue;
                        break;
                    case "/": result /= tempValue;
                        break;
                }
            }
        }
        return result;
    }


}
