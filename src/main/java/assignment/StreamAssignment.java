package assignment;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class StreamAssignment {
    // print the count of names which start with B
    // create a list of names which start with C and contains 's'
    // print the total of chars for all names start with 'M'
    public static void main(String[] args) {
        Path filePath =
                Paths.get("/Users/hoangnguyen/IdeaProjects/lambda-streams-practice/src/myFiles/names.txt");
        try{
            List<String> names = Files.readAllLines(filePath);
            long bCount = names.stream()
                    .filter(name -> name.startsWith("B"))
                    .count();

            Predicate<String> startWithC = s -> s.startsWith("C");
            Predicate<String> startWithM = s -> s.startsWith("M");
            Predicate<String> containsS = s -> s.toLowerCase(Locale.ROOT).contains("s");
            Predicate<String> rule = startWithC.and(containsS);
            Predicate<String> minus = s -> s.contains("-");


            List<String> startWithCAndContainsS = names.stream()
                    .filter(rule)
                    .collect(Collectors.toList());

            int sum = names.stream()
                    .filter(startWithM)
                    .map(s -> s.length())
                    .mapToInt(i -> i)
                    .sum();
            names.stream()
                    .filter(minus)
                    .map(s -> s.replace("-", " "))
                    .forEach(System.out::println);

            System.out.println(bCount);
            System.out.println(startWithCAndContainsS);
            System.out.println(sum);

            System.out.println(

                    names.stream()
                            .max(Comparator.comparing(s -> s.length()))
                            .get()
            );
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}

