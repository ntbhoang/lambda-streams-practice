package assignment;

import lambda.assignment.MathOperation;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class Calculator {
    private static Map<String, MathOperation> MAP_OPERATION = new HashMap<>();
    private static MathOperation add = (x, y) -> x + y;
    private static MathOperation subtract = (x, y) -> x - y;
    private static MathOperation divide = (x, y) -> x / y;
    private static MathOperation multiply = (x, y) -> x * y;

    private Calculator(){}

    static {
        MAP_OPERATION.put("+", add);
        MAP_OPERATION.put("-", subtract);
        MAP_OPERATION.put("*", multiply);
        MAP_OPERATION.put("/", divide);
    }

    static void addOperation(String name, MathOperation op){
        MAP_OPERATION.put(name, op);
    }

    static int calculate(String expr){
        List<String> expStr = Arrays.asList(expr.split(" "));
        int onScreenNumber = Integer.parseInt(expStr.get(0));
        for(int i = 1; i < expStr.size(); i += 2){
            int enteredNumber = Integer.parseInt(expStr.get(i + 1));
            MathOperation op = MAP_OPERATION.get(expStr.get(i));
            onScreenNumber = perform(onScreenNumber, op, enteredNumber);
        }
        return onScreenNumber;
    }

    private static int perform(int onScreenNumber, MathOperation op, int enteredNumber){
        return op.operate(onScreenNumber, enteredNumber);
    }
}
