package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.function.Predicate;

public class TableDemoPage {
    private final WebDriver driver;

    public TableDemoPage(WebDriver driver) {
        this.driver = driver;
    }

    public void goTo(){
        this.driver.get("https://vins-udemy.s3.amazonaws.com/java/html/java8-stream-table.html");
    }

    public void selectRows(String gender){
        this.driver.findElements(By.tagName("tr"))
                .stream()
                .skip(1)
                .map(tr -> tr.findElements(By.tagName("td")))
                .filter(tdList -> tdList.get(1).getText().equalsIgnoreCase(gender))
                .map(tdList -> tdList.get(3))
                .map(td -> td.findElement(By.tagName("input")))
                .forEach(WebElement::click);
    }

    /*
        Instead of having a `String` parameter, so imagine when there is a requirement change
        from select a specific gender to both, or select `male` come from UK -> which we will add more params
        in the traditional ways. Let add a ```Predicate`` as a param to make the method generic
     */
    public void selectRows(Predicate<List<WebElement>> predicate){
        this.driver.findElements(By.tagName("tr"))
                .stream()
                .skip(1)
                .map(tr -> tr.findElements(By.tagName("td")))
                .filter(predicate)
                .map(tdList -> tdList.get(3))
                .map(td -> td.findElement(By.tagName("input")))
                .forEach(WebElement::click);
    }
}
