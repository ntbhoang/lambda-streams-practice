package runNable;

import com.google.common.util.concurrent.Uninterruptibles;

import java.util.concurrent.TimeUnit;

public class RunnableMain {
    public static void main(String[] args) {
        Runnable r = () -> {
            Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);
            System.out.printf("Runnable here");
        };

        r.run();

        new Thread(r).start();

        System.out.println("Hi");
    }
}
