package regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexMain {
    public static void main(String[] args) {


        Pattern p1 = Pattern.compile("\\d+[+/*-]");
        Matcher m1 = p1.matcher("10+9-4*2/3");
        while(m1.find()){
            System.out.println(m1.group());
        }

    }
}
